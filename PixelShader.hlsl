#define MAX_STEPS 500
#define MAX_DIST 500.0f
#define SURF_DIST 0.01f

//Define a mod as hlsl's works slightly differently
#define goodmod(x, y) ((x) - (y) * floor((x) / (y)))

cbuffer ScreenSizeBuffer : register(b1){
	float2 screenSize;
}

cbuffer TimeBuffer : register(b2){
	float time;
}

float2x2 rotate(float angle){
	float s = sin(angle);
	float c = cos(angle);
	return float2x2(c, -s, s, c);
}

//Operations
float smoothIntersection(float dist1, float dist2, float smoothness){
	float h = clamp( 0.5 - 0.5 * (dist2 - dist1) / smoothness, 0.0, 1.0 );
	return lerp( dist2, dist1, h ) + smoothness * h * (1.0 - h);
}

float opSubtract(float dist1, float dist2){
	return max(-dist2, dist1);
}

//Distance functions
float distPlane(float3 pos){
	return pos.y + 1.0f;
}

float distSphere(float3 pos, float3 spherePos, float sphereRadius){
	return length(spherePos - pos) - sphereRadius;
}

float distBox(float3 pos, float3 boxPos, float3 boxSize){
	float3 q = abs(boxPos - pos) - boxSize;
	return length(max(q, 0.0f)) + min(max(q.x, max(q.y, q.z)), 0.0f);
}

float distEllipsoid(float3 pos, float3 ellipsPos, float3 ellipsSize){
	float3 adjustPos = ellipsPos - pos;
	float k0 = length(adjustPos / ellipsSize);
	float k1 = length(adjustPos / (ellipsSize * ellipsSize));
	return k0 * (k0 - 1.0f) / k1;
}

float distEye(float3 pos, float3 eyePos, float radius){
	//The outer eye
	float3 upperOuterPos = float3(eyePos.x, eyePos.y + radius * 0.5f, eyePos.z);
	float3 lowerOuterPos = float3(eyePos.x, eyePos.y - radius * 0.5f, eyePos.z);

	float distUpper = distSphere(pos, upperOuterPos, radius);
	float distLower = distSphere(pos, lowerOuterPos, radius);

	float outerEye = smoothIntersection(distUpper, distLower, 0.01f);

	//Inner subtraction
	float3 innerSubPos = float3(eyePos.x, eyePos.y, eyePos.z - radius * 0.2f);
	float3 innerSubSize = float3(radius - radius * 0.25f, radius * 0.35f, radius * 0.3f);
	float innerSub = distEllipsoid(pos, innerSubPos, innerSubSize);

	float eyeShell = opSubtract(outerEye, innerSub);

	//Outer pupil
	float outerPupilRadius = radius * 0.3f;
	float3 outerPupilPos = float3(eyePos.x, eyePos.y, eyePos.z - outerPupilRadius * 0.22f);
	float outerPupil = distSphere(pos, outerPupilPos, outerPupilRadius);

	float eyeStructure = min(eyeShell, outerPupil);

	//Inner pupil
	float innerPupilRadius = outerPupilRadius * 0.95f;
	float3 innerPupilPos = outerPupilPos;
	float innerPupil = distSphere(pos, innerPupilPos, innerPupilRadius);

	float eye = opSubtract(eyeStructure, innerPupil);

	//Flatten it
	float3 boxPos = float3(eyePos.x, eyePos.y, eyePos.z - radius * 1.3f);
	float3 boxSize = float3(radius, radius, radius);

	float box = distBox(pos, boxPos, boxSize);

	float flatEye = opSubtract(eye, box);

	return flatEye;
}

float distTetrahedron(float3 pos){
	float scale = 2.0f;
	float spacing = 30.0f;
	int iterations = 50;
	for(int i = 0; i < iterations; i++){
		pos.xy = mul(pos.xy, rotate(time * 0.2f));

		pos.x = abs(pos.x);
		pos.y = abs(pos.y);
		pos.z = abs(pos.z);
		
		if(pos.x - pos.y < 0) { pos.xy = pos.yx; }
		if(pos.x + pos.y < 0) { pos.xy = -pos.yx; }
		if(pos.x - pos.z < 0) { pos.xz = pos.zx; }
		if(pos.x + pos.z < 0) { pos.xz = -pos.zx; }

		pos = pos * scale - spacing * (scale - 1.0f);
	}
	return length(pos) * pow(scale, -float(iterations));
}

struct SceneInfo{
	float sceneDist;
	float eyeDist;
	float tetraDist;
};

SceneInfo getScene(float3 pos){
	float eyeSubSphere = distSphere(pos, float3(0,0,0), 1.0f);
	float tetraSubSphere = distSphere(pos, float3(0.0f, 0.0f, 10.0f), 13.0f);

	pos.z += time * 2.0f;
	pos.z = goodmod(pos.z, 16.0f) - 8.0f;

	float eye = distEye(pos, float3(0.0f, 0.0f, 0.0f), 1.5f);
	float eyeSub = opSubtract(eye, eyeSubSphere);

	pos.z -= pos.z;
	
	float tetra = distTetrahedron(pos);
	float tetraSub = opSubtract(tetra, tetraSubSphere);
	
	SceneInfo info;
	info.sceneDist = min(eyeSub, tetraSub);
	info.eyeDist = eyeSub;
	info.tetraDist = tetraSub;
	
	return info;
}

struct MarchInfo{
	SceneInfo scene;
	float totalDist;
};

MarchInfo march(float3 origin, float3 direction){
	MarchInfo info;

	for(int i = 0; i < MAX_STEPS; ++i){
		float3 pos = origin + direction * info.totalDist;
		info.scene = getScene(pos);

		info.totalDist += info.scene.sceneDist;

		if(info.totalDist > MAX_DIST || info.scene.sceneDist < SURF_DIST){
			break;
		}
	}

	return info;
}

//Lighting
float3 getNormal(float3 pos){
	float distance = getScene(pos).sceneDist;
	float2 epsilon = float2(0.01f, 0.0f);

	float3 normal = distance - float3(	getScene(pos - epsilon.xyy).sceneDist,
										getScene(pos - epsilon.yxy).sceneDist,
										getScene(pos - epsilon.yyx).sceneDist);

	return normalize(normal);
}

float calcDiffusePointLight(float3 pos){
	float3 lightPos = float3(0, 5, 0);
	float3 dirToLight = normalize(lightPos - pos);

	float3 posNormal = getNormal(pos);

	float lightStrength = clamp(dot(posNormal, dirToLight), 0.0f, 1.0f);

	float distToLight = march(pos + posNormal * SURF_DIST * 2.0f, dirToLight).totalDist;
	if(distToLight < length(lightPos - pos)){
		lightStrength *= 0.1f;
	}

	return lightStrength;
}

float4 main(float2 tex : TexCoord) : SV_Target{
	float2 pixelPos = float2(tex.x * screenSize.x, tex.y * screenSize.y);
	float2 uv = (pixelPos - 0.5f * screenSize) / screenSize.y;

	float3 rayOrigin = float3(0.0f, 0.0f, 0.0f);
	float3 rayDirection = normalize(float3(uv.x, uv.y, 1.0f));

	MarchInfo info = march(rayOrigin, rayDirection);
	
	float3 col = float3(0.8f, 0.1f, 0.4f);
	
	if(info.totalDist < MAX_DIST){
		if(info.scene.eyeDist < info.scene.tetraDist){
			col = float3(1.0f, 0.0f, 1.0f);
		}else{
			float distRatio = info.totalDist / (MAX_DIST * 0.15f);
			col = float3(distRatio, 0.2f, 1.0f - distRatio);
		}
		
		float3 currentPos = rayOrigin + rayDirection * info.totalDist;
		col *= calcDiffusePointLight(currentPos);
	}

	return float4(col, 1.0f);
}